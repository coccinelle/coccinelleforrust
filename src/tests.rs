#![cfg(test)]

// mod bindings;
mod disjunctions;
mod dots;
mod lifetimes;
mod macros;
mod minuses;
mod others;
mod parameter;
mod parse_cocci;
mod pluses;
mod ruledependance;
mod transformationtest;
